import datetime
from enum import Enum

from django.conf import settings
from django.contrib import admin
from . import models

admin.site.register(models.PumpingStationSignal)
admin.site.register(models.SignalModel)
admin.site.register(models.MLModelHyperparams)
admin.site.register(models.TypeOfDifferenceDiapason)


class NonEmptyFilter(admin.SimpleListFilter):
    title = 'Пустота диапазонов'
    parameter_name = 'non_empty'

    class Lookups(Enum):
        NON_EMPTY = 'Не пустые диапазоны'
        ONLY_EMPTY = 'Только пустые диапазоны'
        NOT_ERROR = 'Не является ошибкой'

    def lookups(self, request, model_admin):
        return [(lookup.name, lookup.value) for lookup in self.Lookups]

    def queryset(self, request, queryset):
        values = {
            self.Lookups.NON_EMPTY.name: queryset.exclude(range_difference=datetime.timedelta()),
            self.Lookups.ONLY_EMPTY.name: queryset.filter(range_difference=datetime.timedelta()),
            self.Lookups.NOT_ERROR.name: queryset.filter(range_difference__gte=settings.RANGE_NOT_ERROR),
        }
        return values.get(self.value(), queryset)


class EndDIfferenceFilter(admin.SimpleListFilter):
    title = 'Продолжительность диапазонов'
    parameter_name = 'end_difference'

    class Lookups(Enum):
        FIRSR_HOUR = 'меньше 2 часов'
        TWO_HOURS = 'свыше 2 часов'
        SIX_HOURS = 'свыше 6 часов'
        TWELVE_HOURS = 'свыше 12 часов'
        ONE_DAY = 'свыше 1 дня'

    def lookups(self, request, model_admin):
        return [(lookup.name, lookup.value) for lookup in self.Lookups]

    def queryset(self, request, queryset):
        values = {
            self.Lookups.FIRSR_HOUR.name: queryset.filter(range_difference__lte=datetime.timedelta(hours=2)),
            self.Lookups.TWO_HOURS.name: queryset.filter(range_difference__gte=datetime.timedelta(hours=2)),
            self.Lookups.SIX_HOURS.name: queryset.filter(range_difference__gte=datetime.timedelta(hours=6)),
            self.Lookups.TWELVE_HOURS.name: queryset.filter(range_difference__gte=datetime.timedelta(hours=12)),
            self.Lookups.ONE_DAY.name: queryset.filter(range_difference__gte=datetime.timedelta(days=1)),
        }
        return values.get(self.value(), queryset)


@admin.register(models.DifferenceDiapason)
class DifferenceDiapasonAdmin(admin.ModelAdmin):
    list_filter = (NonEmptyFilter, 'is_error', 'start_difference', EndDIfferenceFilter, 'signal')
    list_display = ('signal', 'start_difference', 'range_difference', 'is_error')

    def mark_as_not_error(modeladmin, request, queryset):
        queryset.update(is_error=False, is_checked=True)
    mark_as_not_error.short_description = 'Пометить, как НЕ ошибка'

    actions = [mark_as_not_error]
