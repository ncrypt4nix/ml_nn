import pytz
import datetime
from typing import List
from collections import namedtuple

from django.utils.dateparse import parse_datetime

from backend.flow_prediction.utils.utils import request_to_SCADA

METHOD_GET = 'GET'
METHOD_POST = 'POST'

SCADAValue = namedtuple('SCADAValue', ['id', 'value'])
SCADAValue.deserialize = lambda id, value: SCADAValue(id=int(id), value=float(value))

SCADAValueOverTime = namedtuple('SCADAValueOverTime', ['id', 'date', 'value'])
SCADAValueOverTime.deserialize = (
    lambda id, date, value: SCADAValueOverTime(
        id=int(id),
        date=parse_datetime(date).replace(tzinfo=pytz.UTC),
        value=float(value)
    )
)

SCADA_MAEPercentTolerance = namedtuple('SCADA_MAEPercentTolerance', ['id', 'mae_percent_tolerance'])
SCADA_MAEPercentTolerance.deserialize = (
    lambda id, percent: SCADA_MAEPercentTolerance(id=int(id), mae_percent_tolerance=float(percent))
)

AnalogNameList = namedtuple('AnalogNameList', ['id', 'full_name', 'unit'])
AnalogNameList.deserialize = (
    lambda id, full_name, unit: AnalogNameList(id=int(id), full_name=full_name, unit=unit)
)


def get_values_from_SCADA() -> List[SCADAValue]:
    """Получаем данные для предсказания"""
    response = request_to_SCADA(
        url='/api/scada/values',
        method=METHOD_GET,
    )
    return [SCADAValue(*value) for value in response]


def get_slice_values_from_SCADA(
    signal_list: List[int],
    start_date: datetime.datetime,
    end_date: datetime.datetime = None
) -> List[SCADAValueOverTime]:
    if not end_date:
        end_date = datetime.datetime.now(pytz.utc)
    response = request_to_SCADA(
        url='/api/scada/list/values',
        method=METHOD_GET,
        data={'signal_list': signal_list, 'start_date': start_date, 'end_date': end_date}
    )
    return [SCADAValueOverTime.deserialize(*value) for value in response]


def get_full_mae_tolerance_from_SCADA() -> List[SCADA_MAEPercentTolerance]:
    response = request_to_SCADA(
        url='/api/scada/mae-tolerance',
        method=METHOD_GET,
    )
    return [SCADA_MAEPercentTolerance.deserialize(*value) for value in response]


def get_analog_name_list_from_SCADA() -> List[AnalogNameList]:
    response = request_to_SCADA(
        url='/api/scada/analog_name_list',
        method=METHOD_GET,
    )
    return [AnalogNameList.deserialize(*value) for value in response]


def get_alarm_types() -> List[str]:
    """ Получение списка типов аварий """
    return request_to_SCADA(
        url='/api/scada/alarms/types',
        method=METHOD_GET,
    )
