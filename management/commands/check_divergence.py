import datetime
from django.utils import timezone
from django.core.management.base import BaseCommand, CommandError

from backend.flow_prediction.tasks.check_divergence import CheckDivergence


class Command(BaseCommand):
    help = '''
        Команда, исполняемая по крону.
        Проверяет наличие новых расхождений, между предсказаниями и реальными данными со SCADA.
        Также отправляет данные на MQTT брокер.
    '''
    def handle(self, *args, **options):
        print(f'Start: {timezone.now().strftime("%Y-%m-%d %H:%M:%S")}')
        checker = CheckDivergence()
        if not checker.check_execution(interval=datetime.timedelta(minutes=9)):
            return print(False)
        checker.execute()
        return print(f'Finish: {timezone.now().strftime("%Y-%m-%d %H:%M:%S")}')
