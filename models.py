from django.db import models
from django.db.models.deletion import CASCADE


class TypeOfDifferenceDiapason(models.Model):
    name = models.CharField('Название типа диапазона', max_length=128)

    class Meta:
        verbose_name = 'Тип расходящегося диапазона'
        verbose_name_plural = 'Типы расходящихся диапазонов'

    def __str__(self):
        return f'{self.name}'


class DifferenceDiapason(models.Model):
    signal = models.ForeignKey(PumpingStationSignal, on_delete=CASCADE, verbose_name='Сигнал')
    start_difference = models.DateTimeField('Время начала расхождения')
    range_difference = models.DurationField('Время расхождения')
    is_error = models.BooleanField(default=True)
    is_checked = models.BooleanField(default=False)
    diapason_type = models.ForeignKey(
        TypeOfDifferenceDiapason,
        verbose_name='Тип ошибки',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Расходящийся диапазон'
        verbose_name_plural = 'Расходящиеся диапазоны'
        indexes = [
           models.Index(fields=['start_difference']),
        ]

    def __repr__(self):
        return '{id} = {signal}: {start_difference} - {end_difference} | {range_difference}'.format(
            id=self.id,
            signal=self.signal,
            start_difference=self.start_difference,
            end_difference=self.start_difference + self.range_difference,
            range_difference=self.range_difference
        )

    def __str__(self):
        return '{signal}: {start_difference} - {end_difference} | {range_difference}'.format(
            signal=self.signal,
            start_difference=self.start_difference,
            end_difference=self.start_difference + self.range_difference,
            range_difference=self.range_difference
        )

    def get_absolute_url(self):
        """Нужен больше, просто, для перехода с админки на страницу с графиком"""
        return f'/history_confirm/chart/{self.signal.SCADA_id}'

    @property
    def to_range(self):
        return (self.start_difference, self.start_difference + self.range_difference)

    @property
    def end_difference(self):
        return self.start_difference + self.range_difference
