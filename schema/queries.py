import datetime

import graphene
from django.utils import timezone
from django_celery_beat.models import PeriodicTask
from django.db.models import (
    Count,
    F,
    Q,
    Prefetch,
    Case,
    Value,
    When,
    FloatField,
)
from django.conf import settings

from backend.flow_prediction.schema.types import PumpingStationSignalType
from backend.flow_prediction.models import (
    PredictionNote,
    PumpingStationSignal,
    DifferenceDiapason,
    MLModelHyperparams,
)


class Query(graphene.ObjectType):
    get_station_list = graphene.List(PumpingStationSignalType)

    def resolve_get_station_list(root, info):
        now = timezone.now() - settings.TEST_SHIFT
        values = get_values_from_SCADA()
        MLModelHyperparams.sync_from_scada()
        try:
            # Нужен для получения интервала связанных предсказаний
            delta_time_qs = PeriodicTask.objects.filter(
                task='check_value_task',
                interval__period='minutes'
            ).only('interval__every')
            interval_every = datetime.timedelta(minutes=delta_time_qs.earliest('interval__every').interval.every)
        except PeriodicTask.DoesNotExist:
            interval_every = datetime.timedelta(minutes=10)

        return PumpingStationSignal.objects.annotate(
            alarm_count=Count(
                'differencediapason',
                filter=(
                    Q(differencediapason__is_checked=False) &
                    Q(differencediapason__range_difference__gt=settings.RANGE_NOT_ERROR)
                )
            ),
            current_consumption=Case(
                *[When(SCADA_id=item.id, then=Value(item.value)) for item in values],
                default=Value(None),
                output_field=FloatField()
            )
        ).prefetch_related(
            Prefetch(
                'ml_model_hyperparams',
                queryset=PumpingStationSignal.related_qs_to_signal(
                    MLModelHyperparams.objects.order_by('-created_date').select_related('signal')
                ),
                to_attr='_hyperparams'
            ),
            Prefetch(
                'prediction_note',
                queryset=(
                    PredictionNote.objects.filter(
                        record_time__range=(now - interval_every, now)
                    ).order_by('-record_time')
                ),
                to_attr='_predicts'
            ),
            Prefetch(
                'differencediapason_set',
                queryset=PumpingStationSignal.related_qs_to_signal(
                    DifferenceDiapason.objects.filter(
                        is_checked=False,
                        range_difference__gte=settings.RANGE_NOT_ERROR
                    ).order_by('-start_difference')
                ),
                to_attr='_unconfirmed_diapasons'
            ),
            Prefetch(
                'differencediapason_set',
                queryset=DifferenceDiapason.objects.filter(
                    is_checked=False,
                    range_difference__gte=settings.RANGE_NOT_ERROR,
                    start_difference__gte=now - F('range_difference') - settings.RANGE_NOT_ERROR,
                ).order_by('-start_difference'),
                to_attr='_current_diapasons'
            ),
        ).order_by('full_name')
