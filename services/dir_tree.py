import json
from backend.template_path.models import TemplatesDir, Template
from backend.template_path.serializers import TemplatesDirSerializer, TemplateWithKindSerializer


def get_tree_dirs(is_with_templates=False, kinds=None):
    queryset = TemplatesDir.objects.filter(level=0).get_descendants(include_self=True).order_by('level')
    queryset = queryset.prefetch_related('template_set')

    tree = {
        None: {
            'children': [],
            'templates': (
                [
                    TemplateSerializer(i).data
                    for i in Template.objects.filter(dir=None)
                    if not kinds or i.template_kind in kinds
                ]
                if is_with_templates
                else []
            )
        }
    }
    for node in queryset:
        data = TemplatesDirSerializer(node).data
        tree[node.id] = data
        tree[node.id]['templates'] = (
            [
                TemplateWithKindSerializer(i).data
                for i in node.template_set.all()
                if not kinds or i.template_kind in kinds
            ]
            if is_with_templates
            else []
        )
        tree[node.id]['children'] = []

    for node in queryset:
        current_note = tree[node.id]
        parent_node = tree[node.parent_id]
        parent_node['children'].append(current_note)

    return json.dumps(tree[None])
