import pytz
import bisect
import datetime
import logging
import pandas as pd
from collections import namedtuple, defaultdict
from typing import NoReturn

from django.core.cache import cache
from django.conf import settings
from django.db.models import F, Prefetch, Max, Q
from django.utils import timezone
from django.utils.functional import cached_property

from backend.flow_prediction.models import DifferenceDiapason, PredictionNote, MLModelHyperparams, PumpingStationSignal
from backend.flow_prediction.utils.sync_to_mqtt import Sync2MQTT
from backend.flow_prediction.external.scada_api import get_slice_values_from_SCADA


logger = logging.getLogger('debug_check_divergence')


class CheckDivergence:
    Diapason = namedtuple('Diapason', ['start_diapason', 'end_diapason'])
    ALARM_TIMEDELTA = datetime.timedelta(minutes=10)
    TASK_NAME = 'check_divergence'
    LAST_ATTEMPT_KEY = 'check_divergence_attempt_time'

    class DiapasonFinder:
        ExistedDiapason = namedtuple('ExistedDiapason', ['end_difference', 'item'])
        diapasons = defaultdict(list)
        # 5 минут - абсолютно субъективное значение
        gluing_distance = datetime.timedelta(minutes=5)

        class LazyItemKeys(defaultdict):
            """Ленивый генератор ключей для бинарного поиска диапазона"""
            def __missing__(self, key):
                value = self.default_factory(key)
                self[key] = value
                return value

        def __init__(self, queryset):
            _queryset = queryset.only('signal__SCADA_id', 'start_difference', 'range_difference')
            for item in _queryset:
                self.diapasons[item.signal.SCADA_id].append(
                    self.ExistedDiapason(item.end_difference, item)
                )
            for signal in self.diapasons:
                self.diapasons[signal].sort(key=lambda x: x.end_difference)

            # Функция вычисления не в init LazyItemKeys из-за контекста self
            self.item_keys = self.LazyItemKeys(
                lambda scada_id: [item.end_difference for item in self.diapasons[scada_id]]
            )

        def find_by_scada_id(self, diapason, scada_id):
            diapason_list = self.diapasons[scada_id]
            item_keys = self.item_keys[scada_id]
            # Смещение нужно для захвата разрывов
            start_to_find_diapason = diapason.start_diapason - self.gluing_distance
            try:
                item = diapason_list[bisect.bisect_right(item_keys, start_to_find_diapason)].item
            except IndexError:
                return None
            if diapason.end_diapason < item.start_difference:
                return None
            return item

    def __init__(self, start_date: datetime.datetime = None, end_date: datetime.datetime = None):
        self.start_date = start_date or (timezone.now() - datetime.timedelta(hours=2))
        self.end_date = end_date or timezone.now()
        self.time_to_alarm = timezone.now() - self.ALARM_TIMEDELTA
        assert self.start_date < self.end_date, 'Указан неверный диапазон дат при построении расходящихся диапазонов'

    def check_execution(self, interval: datetime.timedelta) -> bool:
        timedelta_error = datetime.timedelta(minutes=1)
        if not self.last_attempt:
            return True
        return (
            self.last_attempt - timedelta_error
            < timezone.now() - interval
        )

    @cached_property
    def last_attempt(self):
        last_attempt_cached = cache.get(self.LAST_ATTEMPT_KEY, None)
        if not last_attempt_cached:
            return None
        return datetime.datetime.fromtimestamp(last_attempt_cached, tz=pytz.utc)

    def execute(self) -> NoReturn:
        logger.info('=' * 25)
        logger.info(f'New Execute: {self.start_date} - {self.end_date}')
        cache.set(self.LAST_ATTEMPT_KEY, int(timezone.now().timestamp()))
        # Переписать запрос, сигналы могут пересекаться
        qs_to_confidence_corridors = PumpingStationSignal.objects.prefetch_related(
            Prefetch(
                'ml_model_hyperparams',
                queryset=(
                    MLModelHyperparams.objects.filter(
                        created_date__gte=self.start_date,
                        created_date__lte=self.end_date
                    )
                    | MLModelHyperparams.objects.filter(
                        # Такой трюк нужен, чтобы победить:
                        # ProgrammingError: column "flow_prediction_mlmodelhyperparams.id"
                        # must appear in the GROUP BY clause or be used in an aggregate function
                        id__in=MLModelHyperparams.objects.annotate(
                            _max=Max(
                                'signal__ml_model_hyperparams__created_date',
                                filter=Q(created_date__lte=self.start_date)
                            )
                        ).filter(created_date=F('_max'))
                    )
                ),
                to_attr='_hyperparams'
            )
        )
        hyperparams_by_scada_id = {
            i.SCADA_id: sorted(i._hyperparams, key=lambda x: x.created_date)
            for i in qs_to_confidence_corridors
        }

        prediction_annotate = {
            'scada_id': F('signal__SCADA_id'),
            'date': F('record_time'),
            'prediction_value': F('value')
        }
        prediction_qs = PredictionNote.objects.filter(
            record_time__range=(self.start_date, self.end_date)
        ).annotate(**prediction_annotate)
        prediction_df = pd.DataFrame(prediction_qs.values(*prediction_annotate))
        real_data = defaultdict(list)
        values_from_scada = get_slice_values_from_SCADA(
            signal_list=prediction_df.scada_id.unique().tolist(),
            start_date=self.start_date,
            end_date=self.end_date
        )
        for value in values_from_scada:
            real_data[value.id].append({'date': value.date, 'real_value': value.value})
        real_df = pd.DataFrame(columns=['date', 'real_value', 'scada_id'])
        # Вычисление скользящего среднего, сгруппированные по scada_id
        for scada_id, values in real_data.items():
            _df = pd.DataFrame(values).resample('10min', on='date').mean().reset_index()
            _df['scada_id'] = scada_id
            real_df = real_df.append(_df)

        # Объединение двух DataFrame
        df = prediction_df.merge(real_df, left_on=['scada_id', 'date'], right_on=['scada_id', 'date'], how='outer')

        def find_confidence_corridor_on_hyperparams(row):
            params = hyperparams_by_scada_id[row['scada_id']]
            if not params:
                return 0.
            keys = tuple(i.created_date for i in params)
            index = bisect.bisect(keys, row['date'])
            if index:
                index -= 1
            return params[index].confidence_corridor

        df['confidence_corridor'] = df.apply(lambda row: find_confidence_corridor_on_hyperparams(row), axis=1)
        df = df.groupby('scada_id').apply(lambda group: (group.set_index('date').sort_index()))
        # Вычисляем расходящиеся диапазоны
        df['is_error'] = df.confidence_corridor < abs(df.real_value - df.prediction_value)
        # scada_divergence = {'scada_id': [[pd.Timestamp]]}
        scada_divergence = {}
        # Заполняем списки точками с расхождениями
        for index, row in df.iterrows():
            # main_list - список списков
            main_list = scada_divergence.setdefault(index[0], [[]])
            if row.is_error:
                # Добавляем элементы к последнему списку из основного списка
                main_list[-1].append(index[1])
            else:
                # Добавляем к основному списку пустой список, к которому потом будут добавляться элементы при ошибке
                main_list.append([])
        # Вычищаем пустые списки и создаем из точек диапазоны
        to_update = []
        to_create = []
        signals = {
            signal.SCADA_id: signal
            for signal in PumpingStationSignal.objects.filter(SCADA_id__in=scada_divergence.keys()).only('id')
        }
        finder_by_existed_qs = self.DiapasonFinder(
            DifferenceDiapason.objects.filter(
                start_difference__gte=self.start_date - F('range_difference'),
                start_difference__lt=self.end_date,
            )
        )
        for scada_id, points_diapasons in scada_divergence.items():
            for points in points_diapasons:
                if not points or len(points) < 2:
                    continue
                diapason = self.Diapason(
                    start_diapason=points[0].to_pydatetime(),
                    end_diapason=points[-1].to_pydatetime()
                )
                # Поиск среди текущих диапазонов
                existed_diapason = finder_by_existed_qs.find_by_scada_id(diapason, scada_id)
                if existed_diapason:
                    start_diapason = min(diapason.start_diapason, existed_diapason.start_difference)
                    end_diapason = max(diapason.end_diapason, existed_diapason.end_difference)
                    this_range = end_diapason - start_diapason
                    if existed_diapason.range_difference == this_range or this_range < settings.RANGE_NOT_ERROR:
                        continue
                    logger.info(f'Before Update: {repr(existed_diapason)}')
                    existed_diapason.range_difference = this_range
                    existed_diapason.start_difference = start_diapason
                    logger.info(f'After Update: {repr(existed_diapason)}')
                    to_update.append(existed_diapason)
                else:
                    range_diapason = diapason.end_diapason - diapason.start_diapason
                    if range_diapason < settings.RANGE_NOT_ERROR:
                        continue
                    new_diapason = DifferenceDiapason(
                        signal=signals.get(scada_id),
                        start_difference=diapason.start_diapason,
                        range_difference=range_diapason
                    )
                    logger.info(f'Create: {repr(new_diapason)}')
                    to_create.append(new_diapason)
        # Обновление существующих диапазонов и создание новых
        DifferenceDiapason.objects.bulk_create(to_create)
        DifferenceDiapason.objects.bulk_update(to_update, ['start_difference', 'range_difference'])
        self.send_alarms(df)

    def send_alarms(self, _df: pd.DataFrame):
        MLModelHyperparams.sync_from_scada()
        df = _df.droplevel(0).reset_index()
        # Ограничим DataFrame ближайшим временем
        df = df[self.time_to_alarm < df['date']]
        df = df[df.date.isin(df.groupby('scada_id', as_index=False)['date'].max().date)]
        # scada_id внезапно float типа
        df.scada_id = df.scada_id.astype(int)
        _json = {
            row.scada_id: {
                'time': int(row.date.timestamp()),
                'predict': row.prediction_value,
                'is_error': row.is_error,
                'reliable_predict': True,
            }
            for index, row in df.iterrows()
        }
        with Sync2MQTT() as mqtt:
            mqtt.execute(_json)
