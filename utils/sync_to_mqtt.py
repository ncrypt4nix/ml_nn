import json
import paho.mqtt.client as mqtt

from django.utils import timezone
from django.conf import settings

from backend.flow_prediction.models import PumpingStationSignal


class Sync2MQTT:
    def __init__(self):
        self.client = mqtt.Client(settings.MQTT_CLIENT_NAME)

    def __enter__(self):
        self.client.connect(settings.MQTT_SERVICE_NAME, port=settings.MQTT_SERVICE_PORT, keepalive=60)
        return self

    def __exit__(self, type, value, traceback):
        self.client.disconnect()

    def execute(self, data):
        self.client.loop_start()
        time_now = int(timezone.now().timestamp())
        default_json = {
            scada_id: {
                'time': time_now,
                'predict': 0,
                'is_error': False,
                'reliable_predict': False,
            }
            for scada_id in PumpingStationSignal.objects.values_list('SCADA_id', flat=True)
        }
        # push по for требует бизнес
        for signal, _json in default_json.items():
            self.client.publish(
                '/'.join([settings.MQTT_SIGNAL_PREFIX, str(signal)]),
                json.dumps(data.get(signal, _json))
            )

        self.client.loop_stop()
