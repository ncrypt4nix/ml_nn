import base64
import json
import requests
import logging
from requests.exceptions import HTTPError

from django.conf import settings
from django.core.exceptions import RequestAborted
from django.core.serializers.json import DjangoJSONEncoder

from backend.core.celery import app
from backend.flow_prediction.models import PumpingStationSignal


logger = logging.getLogger('production')


def request_to_SCADA(url: str, method: str, data: dict = {}):
    """API запрос на сервер SCADA"""
    def non_existing_method():
        raise RequestAborted('405 Method Not Allowed')

    TIMEOUT = 15 * 60

    url = '/'.join([
        settings.API_ODBC_ADDRESS.rstrip('/'),
        url.lstrip('/')
    ])
    data = json.dumps(data, cls=DjangoJSONEncoder)
    headers = {
        'X-ODBC-TOKEN': settings.X_ODBC_TOKEN,
        'X-ODBC-KEY': settings.X_ODBC_KEY,
    }
    methods = {
        'POST': lambda: requests.post(url, headers=headers, data=data, timeout=TIMEOUT),
        'GET': lambda: requests.get(url, headers=headers, data=data, timeout=TIMEOUT)
    }
    response = methods.get(method, non_existing_method)()

    if response.status_code != 200:
        error_msg = '\n'.join([
            f'SCADA ERROR "{url}" (status code: {response.status_code})',
            f'{response.content}'
        ])
        logger.error(error_msg)
        raise HTTPError(error_msg)
    return response.json()


def get_celery_queue(queue_name):
    decoded_tasks = []

    with app.pool.acquire(block=True) as conn:
        tasks = conn.channel().client.lrange(queue_name, 0, PumpingStationSignal.objects.count())

    for task in tasks:
        _json = json.loads(task)
        _json['body'] = json.loads(base64.b64decode(_json['body']))
        decoded_tasks.append(_json)

    return decoded_tasks
